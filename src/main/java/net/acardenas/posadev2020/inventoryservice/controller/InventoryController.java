package net.acardenas.posadev2020.inventoryservice.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.acardenas.posadev2020.inventoryservice.service.InventoryService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/")
public class InventoryController {

  private final InventoryService inventoryService;

  @GetMapping("/{sku}")
  public ResponseEntity<Double> getQtyAvailable(@PathVariable String sku) {
    log.info("inside getQtyAvailable with SKU = {}", sku);
    return ResponseEntity.ok(inventoryService.getQtyAvailable(sku));
  }
}
