package net.acardenas.posadev2020.inventoryservice.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.acardenas.posadev2020.inventoryservice.domain.Inventory;
import net.acardenas.posadev2020.inventoryservice.exception.NotFoundException;
import net.acardenas.posadev2020.inventoryservice.repository.InventoryRepository;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class InventoryService {

  private final InventoryRepository inventoryRepository;

  public Double getQtyAvailable(String sku) {
    log.info("Finding SKU {}", sku);
    Inventory inventory = inventoryRepository.findById(sku).orElseThrow(NotFoundException::new);
    log.info("Inventory for SKU {} = {}", sku, inventory);
    return inventory.getQtyAvailable();
  }

}
