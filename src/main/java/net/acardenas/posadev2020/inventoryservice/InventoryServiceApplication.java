package net.acardenas.posadev2020.inventoryservice;

import net.acardenas.posadev2020.inventoryservice.domain.Inventory;
import net.acardenas.posadev2020.inventoryservice.repository.InventoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InventoryServiceApplication implements CommandLineRunner {

  @Autowired
  private InventoryRepository inventoryRepository;

  public static void main(String[] args) {
    SpringApplication.run(InventoryServiceApplication.class, args);
  }

  @Override
  public void run(String... args) throws Exception {
    inventoryRepository.save(Inventory.builder()
      .sku("sku1")
      .qtyAvailable(1000D).build());
  }
}
