package net.acardenas.posadev2020.inventoryservice.repository;

import net.acardenas.posadev2020.inventoryservice.domain.Inventory;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface InventoryRepository extends MongoRepository<Inventory, String> {

}
