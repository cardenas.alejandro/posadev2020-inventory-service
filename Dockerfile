FROM adoptopenjdk/openjdk11:alpine-jre
RUN apk add tzdata
ADD target/inventory-service.jar inventory-service.jar
RUN mkdir -p /applogs
VOLUME ["/applogs"]
CMD [ "java", "-jar", "/inventory-service.jar" ]