# POSADEV 2020 - Inventory Service

El proposito de este servicio es manejar lo referente al dominio del inventario de productos

Para iniciarlo enviando los logs en formato JSON a la consola:

```shell
docker-compose -f docker-compose-local.yml up 
```
Para iniciarlo creando logs planos y en formato json:

```shell
docker-compose up 
```

Puedes agregar `-d` al final para ejecutarlo en modo detached

Es importante cambiar el volume para que apunte a un directorio real en la maquina host:

```yaml
    volumes:
      - ~/posadev2020logs:/applogs:rw
```

Puedes probar el servicio:

```shell
curl -v -X GET "<host>:<port>/sku1"
```

Tambien puedes accesar atraves del API-Gateway:

```shell
curl -v -X GET "localhost:8080/inventory/sku1"
```